<?php
class Carrito extends Producto{
    public $cart = array();
    public function __construct(){
        parent:: __construct();
        if(isset($_SESSION['cart'])){//si ya esta creada una secion la recarga
            $this->cart= $_SESSION['cart'];
        }
    }
    public function addItem($id,$cantidad){
        $buscar=$this->searchCode($id);
        if($buscar > 0){
            $id = $this->id;
            $nombre = $this->nombre;
            $precio = $this->precio;
            $item = array(
                'id'=>$id,
                'nombre'=>$nombre,
                'precio'=>$precio,
                'cantidad'=>$cantidad,
            );
            if(!empty($this->cart)){
                foreach($this->cart as $key){
                    if($key['id']==$id){
                        $item['cantidad']=$key['cantidad']+$item['cantidad'];
                    }
                }
            }      
            $item['subtotal']=$item['precio']*$item['cantidad'];
            $_SESSION['cart'][$id]=$item;
            $this->updateCart();
        }
    }
    public function removeItem($code){
        unset($_SESSION['cart'][$code]);
        $this ->updateCart();
        return true;
    }
    public function getItems(){
        $html='';
        if(!empty($this->cart)){
            foreach($this->cart as $key){
                $code="'".$key['id']."'";
                $html.='
                        <tr>
                        <td>'.$key['nombre'].'</td>
                        <td>'.number_format($key['precio'],2).'</td>
                        <td>'.$key['cantidad'].'</td>
                        <td>'.number_format($key['subtotal'],2).'</td>
                        <td>
                        <button class="btn btn-primary btn-sm" onClick="deleteProducto('.$code.');"> Eliminar</button>
                        </td>
                        </tr>';
            }
        }
        return $html;
    }
    
    public function saveBuy($array){
        $nombre=$array["modalNombre"];
        $correo=$array["modalEmail"];
        $totp=$array["totA"];
        $tot=$array["tot"];
        $sql = $this->db->query("INSERT INTO carrito (nombre,correo,totalP,total) VALUES ('$nombre','$correo','$totp','$tot')");
        if($this ->db ->connect_errno){
            echo "error de conexion".$this->db->connect_errno;
            return;
        }else{
            echo "datos incertados";
        }
    }

    public function getTotalItems(){
        $total=0;
        if(!empty($this->cart)){
            foreach($this->cart as $key){
                $total +=$key["cantidad"];
            }
        }
        return $total;
    }
    public function getTotal(){
        $total=0;
        if(!empty($this->cart)){
            foreach($this->cart as $key){
                $total +=$key['subtotal'];
            }
        }
        return number_format($total,2);
    }
    public function updateCart(){
        self::__construct();
    }
}
?>