<?php
include("conexion.php");
class Producto extends Model{
    public $id;
    public $nombre;
    public $imagen;
    public $precio;

    public function __construct(){
        parent::__construct();
    }
    public function getProductos(){
        $sql= $this->db->query("SELECT * FROM items");
        $html='';
        foreach($sql->fetch_all(MYSQLI_ASSOC) as $key){
            $code="'".$key['id']."'";
            $html .='
                    <div class="card hovercard">
                        <div class="cardheader">
                            <div class="avatar">
                                <div class="imagen"><img src="img/'.$key['imagen'].'"/></div>
                            </div>
                        </div>
                        <div class="card-body info">
                            <div class="title">
                                <a>'.$key['nombre'].'</a>
                            </div>
                            <div>
                                <a>$'.$key['precio'].'COP</a>
                            </div>
                            <div>
                                <input type="number" id="cantidad" value="1" min="1">
                            </div>
                            <div class="card-footer bottom">
                            <button type="button" class="btn btn-primary btn-sm" onclick="addProducto('.$code.')">Agregar al carrito</button>
                            </div>  
                        </div>
                    </div>
                    ';
        }
        return $html;
    }
    public function searchCode($code){
        $sql= $this->db->query("SELECT * FROM items WHERE id='$code'");
        $producto=$sql->fetch_all(MYSQLI_ASSOC);
        $estado=0;
        foreach($producto as $key){
            $this->id = $key['id'];
            $this->nombre = $key['nombre'];
            $this->imagen = $key['imagen'];
            $this->precio = $key['precio'];
            $estado++;
        }
        return $estado;
    }
}
?>