<?php 
    include("clases/producto.php");
    include("clases/carrito.php");
    $producto = new Producto();
    $cart= new Carrito();
    if(isset($_POST['modalNombre'])){
        $cart->saveBuy($_POST);
    }

    if(isset($_GET['action'])){
        switch($_GET['action']){
            case 'add':
                $cart->addItem($_GET['code'],$_GET['cantidad']);
            break;
            case 'remove':
                $cart->removeItem($_GET['code']);
            break;
        }
    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>CARRITO DIGIT4L</title>
    <script src="js/main.js"></script>
</head>
<body>
    <nav>
    </nav>
    <!-- Tatal del carrido-->
    <center>
    <div class="content">
        <table class="table-carrito">
            <thead class="cartHeader">
            <tr>
                <th colspan="6"> CARRITO </th>
                <th>
                    <a data-target="#contacto"  data-toggle="modal"  type="button" href="#" class=" pagar btn btn-outline-success"> Pagar</button>
                </th>
                
            </tr>
            <tr>
                <th colspan="3">Total a pagar =<?php echo $cart->getTotal();?></th>
                <th colspan="3">Total de items = <?php echo $cart->getTotalItems();?></th>
            </tr>
            </thead>
            <tbody class="cartBody table-carrito">
                <tr>
                    <th> Producto </th>
                    <th> Precio</th>
                    <th>Cantidad</th>
                    <th> Subtotal</th>
                    <th>Opcion</th>
                </tr>
                <?=$cart->getItems();?>
            </tbody>
        </table>
    </div>
    </center>
        <div class="container mt3">
            <div class="row">
                    <?=$producto->getproductos();?>
            </div>
        </div>
    

<!--Modal para pago-->
<div class="modal fade" id="contacto" tabindex="-1" role="dialog" aria-labelledby="comprarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal header">
                <h5 class="modal-title" id="comprarModalLabel"> PAGAR</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  class="form-group" action="index.php" method="POST">
                    <label for="modalNombre" class="col-form-label">Tu Nombre</label>
                    <input type="text" class="form-control" id="modalNombre" name="modalNombre">

                    <label for="modalEmal" class="col-form-label">Tu Correo</label>
                    <input type="email" class="form-control" id="modalEmail" name="modalEmail">

                    <input readonly type="label" name="tot" value="<?php echo $cart->getTotal();?>" class="col-form-label"> $ Total a pagar</label>

                    <input readonly type="label" name="totA" value="<?php echo $cart->getTotalItems();?>" class="col-form-label"> # Total de productos</label>

                    <button type="submit"  class="btn btn-success"> Enviar Factura</button>
                </form>
            </div>
            
        </div>
    </div>
</div>
    

    <!--jQuery, Popper.js, and Bootstrap JS    -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

</body>
</html>